# PyuTest

Pyu's Minetest Game

![screenshot](./screenshot.png)

## Notes so I can remember some parts of the Minetest API

### Groups

Group     Breakable By

crumbly = shovel

cracky = pickaxe

snappy = any, shears

choppy = axe

fleshy = sword

explody = explosions

oddly_breakable_by_hand = any

## Credits

Sounds generated with JFXR

Some textures made and editied [Estrella](https://www.youtube.com/@xoxoEstrella/featured)

Thanks to Estrella for teaching me how I can make better pixel art!

Programming and some textures done by me

## License

Project source code and textures licensed under the GNU LGPL License
