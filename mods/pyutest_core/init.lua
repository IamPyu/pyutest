Translate = minetest.get_translator("pyutest_core")
PyuTestCore = {
  BLOCK_BREAKABLE_INSTANT = 0,
  BLOCK_BREAKABLE_NORMAL = 1,
  BLOCK_BREAKABLE_LONG = 2,
  BLOCK_BREAKABLE_FOREVER = 3,

  util = {
    toint = function (float)
      return tonumber(string.format("%d", float))
    end
  }
}

PyuTestCore.get_schem_path = function (name)
  return minetest.get_modpath("pyutest_core") .. "/schematics/"..name..".mts"
end
PyuTestCore_Path = minetest.get_modpath("pyutest_core")

dofile(PyuTestCore_Path.."/utils.lua") -- Utilities

-- Core Game Code
dofile(PyuTestCore_Path.."/blocks.lua")
dofile(PyuTestCore_Path.."/mapgen.lua")
dofile(PyuTestCore_Path.."/abms.lua")
dofile(PyuTestCore_Path.."/tools.lua")
dofile(PyuTestCore_Path.."/player.lua")
dofile(PyuTestCore_Path.."/crafts.lua")
