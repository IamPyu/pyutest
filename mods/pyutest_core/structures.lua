minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"pyutest_core:grass_block"},
  sidelen = 16,
  fill_ratio = 0.00004,
  biomes = {"grassland"},
  y_max = PyuTestCore_BiomeTops.grassland,
  y_min = 1,
  schematic = PyuTestCore.get_schem_path("hut"),
  rotation = "random",
  flags = "place_center_x, place_center_z"
})

minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"pyutest_core:snow_block"},
  sidelen = 16,
  fill_ratio = 0.00004,
  biomes = {"frozen_plains"},
  y_max = PyuTestCore_BiomeTops.frozen_plains,
  y_min = 1,
  schematic = PyuTestCore.get_schem_path("igloo"),
  rotation = "random",
  flags = "place_center_x, place_center_z"
})

minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"pyutest_core:snow_block"},
  sidelen = 16,
  fill_ratio = 0.00007,
  biomes = {"frozen_plains"},
  y_max = PyuTestCore_BiomeTops.frozen_plains,
  y_min = 1,
  schematic = PyuTestCore.get_schem_path("snowycamp"),
  rotation = "random",
  flags = "place_center_x, place_center_z"
})

minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"pyutest_core:sand_block"},
  sidelen = 16,
  fill_ratio = 0.00006,
  biomes = {"desert"},
  y_max = PyuTestCore_BiomeTops.desert,
  y_min = 1,
  schematic = PyuTestCore.get_schem_path("desertwell"),
  rotation = "random",
  flags = "place_center_x, place_center_z"
})
