PyuTestCore.create_explosion = function (pos, range, rm_pos)
  if rm_pos then
    minetest.remove_node(pos)
  end

  for dx = -range, range do
    for dz = -range, range do
      for dy = -range, range do
        local npos = {x = pos.x + dx, y = pos.y + dy, z = pos.z + dz}
        if minetest.get_node(npos).name == "pyutest_core:tnt" then
          minetest.get_node_timer(npos):start(0.2)
        else
          minetest.dig_node(npos)
        end
      end
    end
  end

  local r = range
  local minpos = {x = pos.x - r, y = pos.y - r, z = pos.z - r}
  local maxpos = {x = pos.x + r, y = pos.y + r, z = pos.z + r}

  minetest.add_particlespawner({
    amount = range * 3,
    time = 1,
    minexptime = 1,
    maxexptime = 1,
    minsize = 32,
    maxsize = 64,

    collisiondetection = false,
    texture = "blast.png",

    minpos = minpos,
    maxpos = maxpos,
  })

  minetest.sound_play("block_break", {
    pos = pos,
    gain = 1.5
  })
end
