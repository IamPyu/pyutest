minetest.register_abm({
  label = "Sponge Loop",
  nodenames = {"pyutest_core:water_source", "pyutest_core:water_flowing"},
  neighbors = {"pyutest_core:sponge"},
  interval = 0.1,
  chance = 1,
  action = function (pos)
    minetest.remove_node(pos)
  end
})

minetest.register_abm({
  label = "Contagious Acid Spread",
  nodenames = PyuTestCore.building_blocks,
  neighbors = {"pyutest_core:contagious_acid"},
  interval = 3.4,
  chance = 8.7,
  catchup = true,
  action = function (pos)
    minetest.set_node(pos, {name = "pyutest_core:contagious_acid"})
  end
})
