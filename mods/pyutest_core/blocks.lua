PyuTestCore.make_node_sounds = function(tbl)
  local t = tbl or {}
  t.footstep = t.footstep or {name = "block_walk", gain = 1}
  t.dug = t.dug or {name = "block_break", gain = 0.50}
  t.place = t.place or {name = "block_place", gain = 0.50}
  return t
end

PyuTestCore.make_node = function(nsname, sname, desc, groups, tiles, extra_conf)
  local conf = {
    description = Translate(desc),
    tiles = tiles,
    groups = groups,
    sounds = PyuTestCore.make_node_sounds()
  }

  if extra_conf ~= nil then
    for k, v in pairs(extra_conf) do
      conf[k] = v
    end
  end

  minetest.register_node(nsname, conf)
  minetest.register_alias(sname, nsname)
end

PyuTestCore.node_boxes = {
  CARPET = {
    type = "fixed",
    fixed = {-0.5, -0.5, -0.5, 0.5, -0.45, 0.5}
  },
  SLAB = {
    type = "fixed",
    fixed = {-0.5, -0.5, -0.5, 0.5, 0, 0.5}
  },
  PILLAR = {
    type = "fixed",
    fixed = {-0.15, -0.5, -0.15, 0.15, 0.5, 0.15}
  }
}
PyuTestCore.building_blocks = {}


PyuTestCore.make_colored_blocks = function (color, dcolor, tex, colorspec, cgroups, bdrawtype, light)
  local groups = cgroups or {
    block = PyuTestCore.BLOCK_BREAKABLE_NORMAL
  }
  local id_block = "pyutest_core:"..color.."_block"
  local id_carpet = "pyutest_core:"..color.."_carpet"
  local id_slab = "pyutest_core:"..color.."_slab"
  local id_pillar = "pyutest_core:"..color.."_pillar"

  minetest.register_node(id_block, {
    description = Translate(dcolor.." Block"),
    drawtype = bdrawtype,
    tiles = {tex},
    color = colorspec,
    groups = groups,
    sounds = PyuTestCore.make_node_sounds(),
    paramtype = light and "light" or nil,
    sunlight_propagates = light and true or nil
  })
  table.insert(PyuTestCore.building_blocks, id_block)

  minetest.register_node(id_carpet, {
    description = Translate(dcolor .. " Carpet"),
    tiles = {tex},
    groups = groups,
    color = colorspec,
    drawtype = "nodebox",
    paramtype = "light",
    paramtyp2 = "facedir",
    node_box = PyuTestCore.node_boxes.CARPET,
    sounds = PyuTestCore.make_node_sounds(),
    sunlight_propagates = light and true or nil
  })
  table.insert(PyuTestCore.building_blocks, id_carpet)

  minetest.register_node(id_slab, {
    description = Translate(dcolor.." Slab"),
    tiles = {tex},
    groups = groups,
    color = colorspec,
    drawtype = "nodebox",
    paramtype = "light",
    paramtyp2 = "facedir",
    node_box = PyuTestCore.node_boxes.SLAB,
    sounds = PyuTestCore.make_node_sounds(),
    sunlight_propagates = light and true or nil
  })
  table.insert(PyuTestCore.building_blocks, id_slab)

  minetest.register_node(id_pillar, {
    description = Translate(dcolor.." Pillar"),
    tiles = {tex},
    groups = groups,
    color = colorspec,
    drawtype = "nodebox",
    paramtype = "light",
    paramtyp2 = "facedir",
    node_box = PyuTestCore.node_boxes.PILLAR,
    sounds = PyuTestCore.make_node_sounds(),
    sunlight_propagates = light and true or nil
  })
  table.insert(PyuTestCore.building_blocks, id_pillar)

  minetest.register_craft({
    output = id_carpet .. " 2",
    recipe = {
      {id_block, id_block}
    }
  })

  minetest.register_craft({
    output = id_slab .. " 3",
    recipe = {
      {id_block, id_block, id_block}
    }
  })

  minetest.register_craft({
    output = id_pillar .. " 3",
    recipe = {
      {id_block},
      {id_block},
      {id_block}
    }
  })
end

PyuTestCore.make_liquid = function (nsname, sname, desc, groups, tiles)
  local function make_liquid_flags(liquidtype)
    local drawtype = ""

    if liquidtype == "source" then
      drawtype = "liquid"
    elseif liquidtype == "flowing" then
      drawtype = "liquid"
    end

    local t = {
      drawtype = drawtype,
      waving = 3,
      walkable = false,
      pointable = false,
      buildable_to = true,
      use_texture_alpha = "blend",
      paramtype = "light",
      drop = "",
      drowning = 1,
      liquidtype = liquidtype,
      liquid_alternative_flowing = nsname.."_flowing",
      liquid_alternative_source = nsname.."_source"
    }
    return t
  end

  PyuTestCore.make_node(nsname.."_source", sname.."_source", desc .. " Source", groups, tiles, make_liquid_flags("source"))
  PyuTestCore.make_node(nsname.."_flowing", sname.."_flowing", "Flowing " .. desc, groups, tiles, make_liquid_flags("flowing"))
end

PyuTestCore.make_colored_blocks("grass", "Grass", "grass.png", nil)
PyuTestCore.make_colored_blocks("dirt", "Dirt", "dirt.png", nil)
PyuTestCore.make_colored_blocks("stone", "Stone", "stone.png", nil, {block = PyuTestCore.BLOCK_BREAKABLE_LONG})
PyuTestCore.make_colored_blocks("iron", "Iron", "iron.png", nil, {block = PyuTestCore.BLOCK_BREAKABLE_LONG})
PyuTestCore.make_colored_blocks("wooden", "Wooden", "wood.png", nil)
PyuTestCore.make_colored_blocks("snow", "Snow", "snow.png", nil)
PyuTestCore.make_colored_blocks("sand", "Sand", "sand.png", nil)
PyuTestCore.make_colored_blocks("sandstone", "Sandstone", "sandstone.png", nil)
PyuTestCore.make_colored_blocks("ice", "Ice", "ice.png", nil)
PyuTestCore.make_colored_blocks("leaves", "Leaves", "leaves.png", nil)
PyuTestCore.make_colored_blocks("mushroom", "Mushroom", "mushroom.png", nil)
PyuTestCore.make_colored_blocks("mushroom_stem", "Mushroom Stem", "mushroom-stem.png", nil)
PyuTestCore.make_colored_blocks("mycelium", "Mycelium", "mycelium.png", nil)
PyuTestCore.make_colored_blocks("hellstone", "Hellstone", "hellstone.png", nil, {block = PyuTestCore.BLOCK_BREAKABLE_LONG})
PyuTestCore.make_colored_blocks("basalt", "Basalt", "basalt.png", nil, {block = PyuTestCore.BLOCK_BREAKABLE_LONG})
PyuTestCore.make_colored_blocks("obsidian", "Obsidian", "obsidian.png", nil, {block = PyuTestCore.BLOCK_BREAKABLE_FOREVER})
PyuTestCore.make_colored_blocks("haybale", "Haybale", "haybale.png", nil)

PyuTestCore.make_colored_blocks("white", "White", "wool.png", "white")
PyuTestCore.make_colored_blocks("red", "Red", "wool.png", "red")
PyuTestCore.make_colored_blocks("orange", "Orange", "wool.png", "orange")
PyuTestCore.make_colored_blocks("yellow", "Yellow", "wool.png", "yellow")
PyuTestCore.make_colored_blocks("green", "Green", "wool.png", "green")
PyuTestCore.make_colored_blocks("blue", "Blue", "wool.png", "blue")
PyuTestCore.make_colored_blocks("purple", "Purple", "wool.png", "purple")
PyuTestCore.make_colored_blocks("black", "Black", "wool.png", "black")
PyuTestCore.make_colored_blocks("pink", "Pink", "wool.png", "hotpink")
PyuTestCore.make_colored_blocks("cherry", "Cherry", "wool.png", "lightpink")

PyuTestCore.make_node("pyutest_core:light", "light", "Light", {
  snappy = 1,
  block = PyuTestCore.BLOCK_BREAKABLE_INSTANT,
  light = 1
}, {
  "light.png"
}, {
  light_source = minetest.LIGHT_MAX,
  on_rightclick = function ()
    if minetest.get_timeofday() >= 0.5 then
      minetest.set_timeofday(0)
    else
      minetest.set_timeofday(0.5)
    end
  end
})

PyuTestCore.make_node("pyutest_core:torch", "torch", "Torch", {
  snappy = 1,
  block = PyuTestCore.BLOCK_BREAKABLE_INSTANT,
  light = 1
}, {
  "torch.png"
}, {
  light_source = 12,
  walkable = false,
  drawtype = "torchlike",
  paramtype = "light",
  inventory_image = "torch.png"
})

PyuTestCore.make_node("pyutest_core:sponge", "sponge", "Sponge", {
  snappy = 1,
  block = PyuTestCore.BLOCK_BREAKABLE_INSTANT
}, {"sponge.png"})

PyuTestCore.make_node("pyutest_core:glass", "glass", "Glass", {
  cracky = 1,
  block = PyuTestCore.BLOCK_BREAKABLE_INSTANT
}, {"glass.png"}, {
  drawtype = "glasslike_framed",
  paramtype = "light",
  sunlight_propagates = true
})

PyuTestCore.make_node("pyutest_core:flower", "rose", "Rose", {
  snappy = 1,
  block = PyuTestCore.BLOCK_BREAKABLE_INSTANT,
}, {"flower.png"}, {
  drawtype = "plantlike",
  walkable = false,
  waving = 1,
  buildable_to = true,
  paramtype = "light",
  sunlight_propagates = true,
  inventory_image = "flower.png"
})

PyuTestCore.make_node("pyutest_core:flower2", "dandelion", "Dandelion", {
  snappy = 1,
  block = PyuTestCore.BLOCK_BREAKABLE_INSTANT,
}, {"flower2.png"}, {
  drawtype = "plantlike",
  walkable = false,
  waving = 1,
  buildable_to = true,
  paramtype = "light",
  sunlight_propagates = true,
  inventory_image = "flower2.png"
})

PyuTestCore.make_node("pyutest_core:flower3", "blue_daisy", "Blue Daisy", {
  snappy = 1,
  block = PyuTestCore.BLOCK_BREAKABLE_INSTANT,
}, {"flower3.png"}, {
  drawtype = "plantlike",
  walkable = false,
  waving = 1,
  buildable_to = true,
  paramtype = "light",
  sunlight_propagates = true,
  inventory_image = "flower3.png"
})

PyuTestCore.make_node("pyutest_core:deadbush", "deadbush", "Deadbush", {
  snappy = 1,
  block = PyuTestCore.BLOCK_BREAKABLE_INSTANT
}, {"deadbush.png"}, {
  drawtype = "plantlike",
  walkable = false,
  waving = 1,
  buildable_to = true,
  paramtype = "light",
  sunlight_propagates = true,
  inventory_image = "deadbush.png"
})

PyuTestCore.make_node("pyutest_core:tree_sapling", "tree_sapling", "Tree Sapling", {
  snappy = 1,
  block = PyuTestCore.BLOCK_BREAKABLE_INSTANT
}, {"sapling.png"}, {
  drawtype = "plantlike",
  walkable = false,
  waving = 1,
  buildable_to = true,
  paramtype = "light",
  sunlight_propagates = true,
  inventory_image = "sapling.png",

  on_timer = function (pos)
    minetest.remove_node(pos)
    pos.y = pos.y - 1
    minetest.place_schematic(pos, PyuTestCore.get_schem_path("tree"), "random", nil, false, "place_center_x, place_center_z")
  end,

  on_rightclick = function (pos)
    local timer = minetest.get_node_timer(pos)
    timer:start(6)
  end
})

PyuTestCore.make_node("pyutest_core:trapdoor", "trapdoor", "Trapdoor", {
  choppy = 1,
  block = PyuTestCore.BLOCK_BREAKABLE_NORMAL
}, {"trapdoor.png"}, {
  drawtype = "nodebox",
  paramtype = "light",
  sunlight_propagates = true,
  node_box = {
    type = "fixed",
    fixed = {-0.5, -0.5, -0.5, 0.5, -0.15, 0.5}
  }
})

PyuTestCore.make_node("pyutest_core:contagious_acid", "acid", "Contagious Acid", {
  fleshy = 1,
  block = PyuTestCore.BLOCK_BREAKABLE_LONG,
}, {"acid.png"}, {})

PyuTestCore.make_node("pyutest_core:barrier", "barrier", "Barrier", {
  block = PyuTestCore.BLOCK_BREAKABLE_FOREVER
}, {}, {
  drawtype = "airlike",
  walkable = true,
  paramtype = "light",
  sunlight_propagates = true,
  inventory_image = "barrier.png",
  wield_image = "barrier.png"
})

PyuTestCore.make_node("pyutest_core:fire", "fire", "Fire", {
  block = PyuTestCore.BLOCK_BREAKABLE_INSTANT
}, {"fire.png"}, {
  drawtype = "firelike",
  walkable = false,
  buildable_to = true,
  paramtype = "light",
  sunlight_propagates = true,
  -- inventory_image = "fire.png"
})

PyuTestCore.make_node("pyutest_core:tnt", "tnt", "TNT", {
  block = PyuTestCore.BLOCK_BREAKABLE_INSTANT
}, {
  "tnt-top-bottom.png",
  "tnt-top-bottom.png",
  "tnt-side.png",
  "tnt-side.png",
  "tnt-side.png",
  "tnt-side.png"
}, {
  on_rightclick = function (pos)
    local timer = minetest.get_node_timer(pos)
    timer:start(4)
  end,

  on_timer = function (pos)
    PyuTestCore.create_explosion(pos, 3, true)
  end
})

PyuTestCore.make_liquid("pyutest_core:water", "water", "Water", {}, {"water.png"})
PyuTestCore.make_liquid("pyutest_core:lava", "lava", "Lava", {}, {"lava.png"})
PyuTestCore.make_liquid("pyutest_core:oil", "oil", "Oil", {}, {"oil.png"})
PyuTestCore.make_liquid("pyutest_core:liquid_acid", "liquid_acid", "Acid", {}, {"acid.png"})
