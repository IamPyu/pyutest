minetest.register_alias("mapgen_stone", "pyutest_core:stone_block")
minetest.register_alias("mapgen_water_source", "pyutest_core:water_source")
minetest.register_alias("mapgen_river_water_source", "pyutest_core:water_source")
minetest.register_alias("mapgen_lava_source", "pyutest_core:water_source")

-- Biomes

PyuTestCore_SurfaceBiomeBottom = -3
PyuTestCore_BiomeTops = {
  grassland = 40,
  desert = 70,
  frozen_plains = 50,
  mountains = 300,
  mushroom_fields = 40,
  hellbounds = 70,
  oillands = 45
}

minetest.register_biome({
  name = "grassland",

  node_top = "pyutest_core:grass_block",
  depth_top = 1,

  node_filler = "pyutest_core:dirt_block",
  depth_filler = 3,

  y_max = PyuTestCore_BiomeTops.grassland,
  y_min = PyuTestCore_SurfaceBiomeBottom,

  heat_point = 50,
  humidity_point = 50,
})

minetest.register_biome({
  name = "forest",

  node_top = "pyutest_core:grass_block",
  depth_top = 1,

  node_filler = "pyutest_core:dirt_block",
  depth_filler = 3,

  y_max = PyuTestCore_BiomeTops.grassland,
  y_min = PyuTestCore_SurfaceBiomeBottom,

  heat_point = 50,
  humidity_point = 63
})

minetest.register_biome({
  name = "stony_mountains",

  node_top = "pyutest_core:stone_block",
  depth_top = 1,

  node_filler = "pyutest_core:stone_block",
  depth_filler = 3,

  y_max = PyuTestCore_BiomeTops.mountains,
  y_min = PyuTestCore_BiomeTops.grassland,

  heat_point = 45,
  humidity_point = 34
})

minetest.register_biome({
  name = "desert",

  node_top = "pyutest_core:sand_block",
  depth_top = 1,

  node_filler = "pyutest_core:sandstone_block",
  depth_filler = 3,

  y_max = PyuTestCore_BiomeTops.desert,
  y_min = PyuTestCore_SurfaceBiomeBottom,

  heat_point = 78,
  humidity_point = 4
})

minetest.register_biome({
  name = "desert_mountains",

  node_top = "pyutest_core:sand_block",
  depth_top = 1,

  node_filler = "pyutest_core:sandstone_block",
  depth_filler = 3,

  y_max = PyuTestCore_BiomeTops.mountains,
  y_min = PyuTestCore_BiomeTops.desert,

  heat_point = 65,
  humidity_point = 8
})

minetest.register_biome({
  name = "snowy_mountains",

  node_top = "pyutest_core:snow_block",
  depth_top = 1,

  node_filler = "pyutest_core:snow_block",
  depth_filler = 3,

  y_max = PyuTestCore_BiomeTops.mountains,
  y_min = PyuTestCore_BiomeTops.frozen_plains,

  heat_point = 16,
  humidity_point = 23
})


minetest.register_biome({
  name = "frozen_plains",

  node_top = "pyutest_core:snow_block",
  depth_top = 1,

  node_filler = "pyutest_core:snow_block",
  depth_filler = 3,

  node_water_top = "pyutest_core:ice_block",
  depth_water_top = 10,

  y_max = PyuTestCore_BiomeTops.frozen_plains,
  y_min = PyuTestCore_SurfaceBiomeBottom,

  heat_point = 19,
  humidity_point = 23
})

minetest.register_biome({
  name = "wasteland",

  node_top = "pyutest_core:dirt_block",
  depth_top = 1,

  node_filler = "pyutest_core:dirt_block",
  depth_filler = 3,

  node_water_top = "pyutest_core:ice_block",
  depth_water_top = 10,

  y_max = PyuTestCore_BiomeTops.frozen_plains,
  y_min = PyuTestCore_SurfaceBiomeBottom,
  heat_point = 21,
  humidity_point = 9
})

minetest.register_biome({
  name = "mushroom_fields",
  node_top = "pyutest_core:mycelium_block",
  depth_top = 1,

  node_filler = "pyutest_core:dirt_block",
  depth_filler = 3,

  y_max = PyuTestCore_BiomeTops.frozen_plains,
  y_min = PyuTestCore_SurfaceBiomeBottom,

  heat_point = 34,
  humidity_point = 76
})

minetest.register_biome({
  name = "hellbounds",

  node_top = "pyutest_core:hellstone_block",
  depth_top = 1,

  node_filler = "pyutest_core:basalt_block",
  depth_filler = 9,

  node_water = "pyutest_core:lava_source",
  node_river_water = "pyutest_core:lava_source",

  node_riverbed = "pyutest_core:lava_source",
  depth_riverbed = 2,

  y_max = PyuTestCore_BiomeTops.hellbounds,
  y_min = PyuTestCore_SurfaceBiomeBottom,

  heat_point = 98,
  humidity_point = 3
})

minetest.register_biome({
  name = "oillands",

  node_top = "pyutest_core:basalt_block",
  depth_top = 1,

  node_filler = "pyutest_core:basalt_block",
  depth_filler = 3,

  node_water = "pyutest_core:oil_source",
  node_river_water = "pyutest_core:oil_source",

  node_riverbed = "pyutest_core:oil_source",
  depth_riverbed = 2,

  y_max = PyuTestCore_BiomeTops.oillands,
  y_min = PyuTestCore_SurfaceBiomeBottom,

  heat_point = 48,
  humidity_point = 18
})


-- Structures
dofile(PyuTestCore_Path.."/structures.lua")

-- Trees, Plants and More
dofile(PyuTestCore_Path.."/trees.lua")
