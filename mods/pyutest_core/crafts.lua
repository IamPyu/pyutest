minetest.register_craft({
  output = "pyutest_core:stick 4",
  recipe = {
    {"pyutest_core:wooden_block"},
    {"pyutest_core:wooden_block"}
  }
})

minetest.register_craft({
  output = "pyutest_core:pickaxe 1",
  recipe = {
    {"pyutest_core:wooden_block", "pyutest_core:wooden_block", "pyutest_core:wooden_block"},
    {"", "pyutest_core:stick", ""},
    {"", "pyutest_core:stick", ""}
  }
})

-- this recipe makes no sense, but who cares?
minetest.register_craft({
  output = "pyutest_core:contagious_acid 3",
  recipe = {
    {"pyutest_core:mushroom_stem_block"},
    {"pyutest_core:dirt_block"}
  }
})

minetest.register_craft({
  output = "pyutest_core:light 4",
  recipe = {
    {"pyutest_core:torch", "pyutest_core:torch"},
    {"pyutest_core:torch", "pyutest_core:torch"}
  }
})
