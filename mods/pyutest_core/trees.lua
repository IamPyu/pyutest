minetest.register_decoration({
  deco_type = "simple",
  place_on = {"pyutest_core:grass_block"},
  sidelen = 16,
  fill_ratio = 0.009,
  biomes = {"forest", "grassland"},
  y_max = PyuTestCore_BiomeTops.grassland,
  y_min = 1,
  decoration = {"pyutest_core:flower", "pyutest_core:flower2", "pyutest_core:flower3"}
})

minetest.register_decoration({
  deco_type = "simple",
  place_on = {"pyutest_core:dirt_block"},
  sidelen = 16,
  fill_ratio = 0.019,
  biomes = {"wasteland", "desert"},
  y_max = PyuTestCore_BiomeTops.grassland,
  y_min = 1,
  decoration = "pyutest_core:deadbush"
})

minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"pyutest_core:grass_block"},
  sidelen = 16,
  fill_ratio = 0.004,
  biomes = {"forest"},
  y_max = PyuTestCore_BiomeTops.grassland,
  y_min = 1,
  schematic = PyuTestCore.get_schem_path("tree"),
  rotation = "random",
  flags = "place_center_x, place_center_z"
})

minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"pyutest_core:grass_block"},
  sidelen = 16,
  fill_ratio = 0.004,
  biomes = {"forest"},
  y_max = PyuTestCore_BiomeTops.grassland,
  y_min = 1,
  schematic = PyuTestCore.get_schem_path("tree2"),
  rotation = "random",
  flags = "place_center_x, place_center_z"
})

minetest.register_decoration({
  deco_type = "schematic",
  place_on = {"pyutest_core:mycelium_block", "pyutest_core:grass_block"},
  sidelen = 16,
  fill_ratio = 0.003,
  biomes = {"mushroom_fields", "forest"},
  y_max = PyuTestCore_BiomeTops.mushroom_fields,
  y_min = 1,
  schematic = PyuTestCore.get_schem_path("mushroom"),
  rotation = "random",
  flags = "place_center_x, place_center_z"
})
