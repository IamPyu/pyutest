PyuTestCore.make_tool = function (nsname, sname, desc, groups, wield_image, extra_conf)
  local conf = {
    description = Translate(desc),
    wield_image = wield_image,
    inventory_image = wield_image,
    groups = groups
  }

  if extra_conf ~= nil then
    for k, v in pairs(extra_conf) do
      conf[k] = v
    end
  end

  minetest.register_tool(nsname, conf)
  minetest.register_alias(sname, nsname)
end

PyuTestCore.make_item = function (nsname, sname, desc, groups, wield_image, extra_conf)
  local conf = {
    description = Translate(desc),
    wield_image = wield_image,
    inventory_image = wield_image,
    groups = groups
  }

  if extra_conf ~= nil then
    for k, v in pairs(extra_conf) do
      conf[k] = v
    end
  end

  minetest.register_craftitem(nsname, conf)
  minetest.register_alias(sname, nsname)
end

PyuTestCore.make_tool("pyutest_core:pickaxe", "pickaxe", "Pickaxe", {}, "pickaxe.png", {
  stack_max = 1,
  tool_capabilities = {
    groupcaps = {
      block = {
        times = {
          [PyuTestCore.BLOCK_BREAKABLE_INSTANT] = 0.025,
          [PyuTestCore.BLOCK_BREAKABLE_NORMAL] = 0.25,
          [PyuTestCore.BLOCK_BREAKABLE_LONG] = 0.45,
          [PyuTestCore.BLOCK_BREAKABLE_FOREVER] = 2
        },
        uses = 600
      }
    }
  }
})

PyuTestCore.make_item("pyutest_core:stick", "stick", "Stick", {}, "stick.png", {
  stack_max = 99
})

PyuTestCore.make_tool("pyutest_core:bomb", "bomb", "Bomb", {}, "bomb.png", {
  on_use = function (_, user)
    if user == nil then
      return
    end
    local pos = user:get_pos()
    PyuTestCore.create_explosion(pos, 2)
  end
})
