-- player privs
minetest.register_on_joinplayer(function(player)
  minetest.set_player_privs(player:get_player_name(), {
    interact = true,
    shout = true,
    fly = true,
    fast = true,
    noclip = true,
    give = true,
    teleport = true,
    bring = true,
    settime = true
  })
end)

-- player hand
minetest.register_item(":", {
  type = "none",
  wield_image = "hand.png"
})

minetest.override_item("", {
  range = 6,
  tool_capabilities = {
    groupcaps = {
      block = {
        times = {
          [PyuTestCore.BLOCK_BREAKABLE_INSTANT] = 0.25,
          [PyuTestCore.BLOCK_BREAKABLE_NORMAL] = 0.4,
          [PyuTestCore.BLOCK_BREAKABLE_LONG] = 4,
          [PyuTestCore.BLOCK_BREAKABLE_FOREVER] = 12
        },
	uses = 0
      }
    },
    punch_attack_uses = 0
  }
})
